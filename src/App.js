import React, { useState } from 'react';
//import logo from './logo.svg';
import './App.css';
import Header from './components/header/Header';
import FilterComponent from './components/filter/FilterComponent';
import Footer from './components/footer/Footer';

const groceries = ['Strawberry','Blueberry','Orange','Banana','Apple','Carrot','Celery','Mushroom',
  'Green Pepper','Eggs','Cheese','Butter','Chicken','Fish','Rice','Pasta'];

function App() {
  const [groceryItems, setGroceryItems] = useState([...groceries ]);
  const [basketItems, setBasketItems] = useState({});
  const [pageType, setPageType] = useState('');

  const filterItems = (filterTerm) => {
    setGroceryItems([...groceries].filter(item => item.toLowerCase().includes(filterTerm.toLowerCase())));
  }

  const addToBasket = (item) => {
    let quantity = basketItems[item] == null? 1 : basketItems[item].quantity+1;
    setBasketItems({...basketItems, [item]: {...basketItems[item], quantity: quantity}});
  }

  const onChangePageType = (type) => {
    setPageType(type);
  }

  const toggleBuy = (item) => {
    let isPurchased  = basketItems[item].isPurchased;
    if(pageType === 'pending' || pageType === 'purchased'){
      const {[item]: removedItem , ...newObject} = basketItems;
      setBasketItems({...newObject});
      return;
    }
    setBasketItems({...basketItems, [item]: {...basketItems[item],  isPurchased: !isPurchased}});
    return 0;
  }

  let basketList = null;

  // checking what to display in basket
  if(pageType === ''){
    basketList = Object.keys(basketItems).map((item, index) => {
      let lineThrough = basketItems[item].isPurchased ? 'line-through': ''
      return (<div 
        className={'list-item ' + (index % 2 === 0 ?'grey': 'off-grey') + ` ${lineThrough}` } 
        onClick={() => toggleBuy(item)}
        key={item}
        >
        <i className="fas fa-minus-square" aria-hidden="true" />{basketItems[item].quantity + ' ' + item}
      </div>)
      })
  }else if(pageType === 'purchased') {
    basketList = Object.keys(basketItems).filter(item => basketItems[item].isPurchased).map((item, index) => {
      let lineThrough = basketItems[item].isPurchased ? 'line-through': ''
      return (<div 
          className={'list-item ' + (index % 2 === 0 ?'grey': 'off-grey') + ` ${lineThrough}` } 
          onClick={() => toggleBuy(item)}
          key={item}
        >
        <i className="fas fa-minus-square" aria-hidden="true" />{basketItems[item].quantity + ' ' + item}
      </div>)
      })
  } else {
    basketList = Object.keys(basketItems).filter(item => !basketItems[item].isPurchased).map((item, index) => {
      let lineThrough = basketItems[item].isPurchased ? 'line-through': ''
      return (<div 
          className={'list-item ' + (index % 2 === 0 ?'grey': 'off-grey') + ` ${lineThrough}` } 
          onClick={() => toggleBuy(item)}
          key={item}
        >
        <i className="fas fa-minus-square" aria-hidden="true" />{basketItems[item].quantity + ' ' + item}
      </div>)
      })
  }

  if(basketList == null) {
    
  }

  return (
    <div className="App">
      <Header />
      <div className="App-filter">
        <FilterComponent filterItems={filterItems}/>
      </div>
      <div className="App-list-container">
        <div className="Groceries" style={{textAlign: 'left'}}>
          <p className="App-bold"><i class="fa fa-leaf" aria-hidden="true"></i>Groceries</p>
          {groceryItems.map((item, index) => (
            <div 
              className={'list-item ' + (index % 2 === 0 ?'grey': 'off-grey')}
              onClick={() => addToBasket(item)}
              key={index}>
              <i className="fas fa-plus-square" aria-hidden="true" />{item}
            </div>
          ))}
        </div>
        
        <div className="Basket" style={{textAlign: 'left'}}>
          <p className="App-bold"><i class="fa fa-shopping-basket" aria-hidden="true"></i>Basket 
            {basketList.length > 0 && (<span><i 
              class="fa fa-trash" 
              aria-hidden="true" style={{textAlign: 'right'}}
            onClick={() => setBasketItems({})}></i></span>)
            }
            </p>

          { basketList.length > 0  ? basketList  : basketList = (<div 
              className="list-item grey">
                <i className="fas fa-minus-square" aria-hidden="true" />Your basket is empty!
              </div>)
          }
        </div>
      </div>
      <Footer onChangePageType={type => onChangePageType(type)} pageType={pageType}/>
    </div>
  );
}

export default App;
