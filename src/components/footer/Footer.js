import React from 'react';
import './Footer.css';

const Footer = (props) => {
    return (
        <div className="Footer-container">
            <p>
                <span 
                    onClick={() => props.onChangePageType('')} 
                    className={props.pageType === '' ? '': 'link-underline'}>All</span>, 
                <span 
                    onClick={() => props.onChangePageType('pending')} 
                    className={props.pageType === 'pending' ?' ' :'link-underline'}>Pending</span>, 
                <span 
                    onClick={() => props.onChangePageType('purchased')}
                    className={props.pageType === 'purchased' ?'': 'link-underline'}>Purchased</span>
            </p>
        </div>
        );
}

export default Footer;