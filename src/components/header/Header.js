import React from 'react';
import './Header.css';

const Header = () => {
    return (
        <div className="App-header">
            <h1 style={{margin: 0}}>
                <i className="fa fa-shopping-basket logo" aria-hidden="true"></i>
            </h1>
            <p className="title">Hello, Basket!</p>
        </div>
    )
}

export default Header;