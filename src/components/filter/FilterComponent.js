import React, { useState } from 'react';
import './FilterComponent.css';

const FilterComponent = (props) => {

    const [filterValue, setFilterValue] = useState('');

    const changeHandler = (event) => {
        setFilterValue(event.target.value);
        props.filterItems(event.target.value);
    }

    return (
        <div className="Filter-container">
            <div className="Filter-input-container">
                <input 
                    placeholder="Filter e.g Apple" 
                    className="Filter-input" 
                    value={filterValue}
                    onChange={changeHandler} />
            </div>
        </div>
    )
}

export default FilterComponent;